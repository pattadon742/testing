
import unittest
from code01.FunnyString import funnyString

class FunnyStringTest(unittest.TestCase):
    
    def test_funnystring(self):
        string = 'acxz'
        isfunny = funnyString(string)
        self.assertEqual('Funny',isfunny)
    
    def test_notfunnystring(self):
        string = 'bcxz'
        isfunny = funnyString(string)
        self.assertEqual('Not Funny',isfunny)
    
if __name__=="__main__":
    unittest.main()