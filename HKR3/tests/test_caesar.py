import unittest
from code03.caesar import caesarCipher

class CaesarCipherTest(unittest.TestCase):
    
    def test_caesar_shift_2(self):
        string = 'middle-Outz'
        shift = 2
        caesarshift = caesarCipher(string,shift)
        self.assertEqual('okffng-Qwvb',caesarshift)
    
    def test_caesar_shift_5(self):
        string = 'Always-Look-on-the-Bright-Side-of-Life'
        shift = 5
        caesarshift = caesarCipher(string,shift)
        self.assertEqual('Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj',caesarshift)
    
if __name__=="__main__":
    unittest.main()